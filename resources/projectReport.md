# Raportti projektin kulusta
## Viikko 1 (30.1. - 5.2.2017)
+ SQL-tietokannan valmistelu ja käsittely:
    + Cloud9 MYSQL käsittely:
        >https://community.c9.io/t/setting-up-mysql/1718  
    https://dev.mysql.com/doc/refman/5.7/en/mysql-batch-commands.html

    + Dump file (tiedosto, jolla tietokanta rakennetaan) esimerkki:
        >http://stackoverflow.com/questions/5535479/create-database-from-dump-file-in-mysql-5-0
+ Tietokantakomentoja
    + Luo käyttäjä:
        ```
        CREATE USER 'jeffrey'@'localhost' IDENTIFIED BY 'new_password';
        ```
    + Anna kaikki oikeudet root käyttäjänä käyttäjälle `admin`, tietokantaan `event_db` ja hostissa `localhost`:
        ```
        GRANT ALL ON event_db.* TO 'admin'@'localhost';
        ```
    + Anna kaikki oikeudet kaikkiin tietokantoihin root käyttäjänä käyttäjälle `admin` ja hostissa `localhost`:
        ```
        GRANT ALL ON *.* TO 'admin'@'localhost';
        ```
    + Salasanan vaihto/asettaminen käyttäjälle `admin` ja salasanalla `newPasword`:
        ```
        SET PASSWORD FOR 'admin'@'localhost' = PASSWORD('myNewPass');
        ```
## Viikko 2 (8.2. - 12.2.2017)
+ Tietokannan tietojen haku:
    + SQL Querylla luodun result-olion muuttaminen JSON-olioksi niin, että siinä näkyy erikoismerkit oikein ja /-merkkien eteen ei tule \\-merkkiä: (Fix markdown)
        ```php
        <?php
        function convertSQLResultToJSON($result) { // $result is MYSQL query request generated object
            // Array of dictionaries
            $dictionaries = array();
            foreach ($result as $row) {
            
                // Collect object field names (key) and field values (value) to a dictionary
                $dictionary = array();
                foreach ($row as $key => $value) {
                    //
                    if (is_numeric($value)) {
                        $value = (int) $value;
                    } else if (gettype($value) == "string") {
                        $value = utf8_encode($value);
                    }
                    $dictionary[$key] = $value;
                }
                array_push($dictionaries, $dictionary);
            }
            $jsonText = json_encode($dictionaries,
                // JSON_UNESCAPED_SLASHES removes \ characters in front of / characters
                // JSON_UNESCAPED_UNICODE makes it use unicode
                JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE); 
            return $dictionaries;
        }
        >
        ```
## Viikko 3 (13.2. - 19.2.)
+ Doctrinen käyttöönotto
    + Composer asennusohjeet:
        >https://www.digitalocean.com/community/tutorials/how-to-install-and-use-composer-on-ubuntu-14-04

    + Doctrinen asennus:
        >http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/configuration.html

    + Aloitus:
        >http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/tutorials/getting-started.html

+ Doctrine käyttö:
    + Tietokantaan yhditäminen:
        ```php
        <?php
        // bootstrap.php tiedosto
        use Doctrine\ORM\Tools\Setup;
        use Doctrine\ORM\EntityManager;
        
        require_once "vendor/autoload.php";
        
        // Create a simple "default" Doctrine ORM configuration for Annotations
        $isDevMode = true;
        $config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/src/classes"), $isDevMode);
        
        // database configuration parameters
        $dbParams = array(
            'driver'   => 'pdo_mysql',
            'user'     => 'admin',
            'password' => 'ryhma10',
            'dbname'   => 'test_db',
        );
        
        // obtaining the entity manager
        $entityManager = EntityManager::create($dbParams, $config);
        ?>
        ```
    + Luokka-annotaatiot:
        ```php
        <?php
        /**
         * @Entity @Table(name="Event")
         **/
        class Event
        {
            /**
             * @var int
             */
            /** @Id @Column(type="integer") @GeneratedValue **/
            protected $id;
            /**
             * @var string
             */
            /** @Column(type="string") **/
            protected $name;
        
            public function getId()
            {
                return $this->id;
            }
        
            public function getName()
            {
                return $this->name;
            }
        
            public function setName($name)
            {
                $this->name = $name;
            }
        }
        ?>
        ```
        >http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/annotations-reference.html
        
    + Tietokannan rakentaminen oliotiedostoista:
        ```
        vendor/bin/doctrine orm:schema-tool:create
        ```
    + Tietokannan uudelleen rakentaminen oliotiedostoista:
        ```
        vendor/bin/doctrine orm:schema-tool:drop --force
        vendor/bin/doctrine orm:schema-tool:create
        ```
        TAI
        ```
        vendor/bin/doctrine orm:schema-tool:update --force
        ```
    + Olioiden lisäys:
        ```php
        <?php
        // Create_Events.php tiedosto kansiossa src
        require_once "../bootstrap.php";
        foreach (glob("classes/*.php") as $filename)
        {
            include $filename;
        }
        
        $newEventName = "Test event name";
        
        $event = new Event();
        $event->setName($newEventName);
        
        $entityManager->persist($event);
        $entityManager->flush();
        
        echo "Created Product with ID " . $event->getId() . "\n";
        
        ?>
        ```
    + Olioiden haku:
        ```php
        <?php
        // Fetch_Events.php tiedosto kansiossa src
        require_once "../bootstrap.php"; //Yhteyden muodostu ja entityManagerin luonti
        
        $eventRepository = $entityManager->getRepository('Event');
        $events = $eventRepository->findAll();
        
        foreach ($events as $event) {
            echo sprintf("-%s\n", $event->getName());
        }
        ?>
        ```
## Viikko 5

+ REST API:n kutsujen kontrolloivan php-tiedosto
    + Lisää tiedosto kansioon, jonka nimi on osa kutsua. Esim. Jos domain osoitteen jälkeen on "RESTAPI", RESTAPI-kansiossa oleva php-tiedosto käsittelee "domain.com/RESTAPI/" jälkeen tulevia kutsuja. Määritä php-tiedosto .htaccess tiedostolla kohtaan, jossa on src/EventController.php:
        ```
        Options -MultiViews
        RewriteEngine On
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteRule ^ src/EventController.php [QSA,L]
        ```
+ Cloud9 PHP päivittäminen 5.5.9 versioon 5.6:
    >http://askubuntu.com/questions/795629/install-php-extensions-in-ubuntu-16-04\
    >https://www.digitalocean.com/community/questions/how-to-upgrade-from-php-v-5-5-9-to-v-5-6
+ PHPDocin asentaminen:
    + Asenna PEAR täältä: 
        >https://pear.php.net/manual/en/installation.getting.php

    + Tämän jälkeen hae phpDocumentator täältä: 
        >https://www.phpdoc.org/ (Asennus täytyy tehdä root oikeuksilla).
    
    + PHPDoc käynnistetään ```phpdoc -d projektin/tiedosto/sijainti -f dokumentoitava/kansio  -t kohde/kansio```, esimerkiksi meillä:
     ```phpdoc -d src/ -f src/EventController.php  -t src/phpdoc/ ```
+ PHPunit käyttö:
    + Esimerkkitesti:
        ```php
        <?php
        declare(strict_types=1);
        
        require(__DIR__."/../src/EventModel.php");
        require(__DIR__."/../src/classes/classes.php");
        
        final class TestEventModel extends PHPUnit_Framework_TestCase
        {
            public function testObjectToJSONConvertion() {
                $model = new EventModel();
                $event = new Event();
                
                $name = "Shitty McShitshit";
                $event->setName($name);
                $dictionary = $model->convertObjectToDictionary($event);
                $this->assertTrue(gettype($dictionary) == "array" && $dictionary["name"] == $name);
            }
            
            public function testObjectToJSONText() {
                $model = new EventModel();
                $event = new Event();
                
                $name = "Skabde ding dong";
                $event->setName($name);
                $text = $model->convertObjectToJSONText($event);
                $this->assertTrue(gettype($text) == "string");
                
                // Check if converts back
                $dictionary = json_decode($text, true); // true means that make array
                $this->assertTrue($dictionary["name"] == $name); //TODO: Ei toimi!
            }
        }

        ```
    + Config tiedosto phpunitille (HUOM! Tämä tiedosto projektin juuressa):
        ```xml
        <?xml version="1.0" encoding="UTF-8"?>
        <phpunit backupGlobals="false"
                 backupStaticAttributes="false"
                 colors="true"
                 convertErrorsToExceptions="true"
                 convertNoticesToExceptions="true"
                 convertWarningsToExceptions="true"
                 processIsolation="false"
                 stopOnFailure="false"
                 syntaxCheck="false"
            >
        
            <filter>
                <whitelist processUncoveredFilesFromWhitelist="true">
                    <file>./src/EventModel.php</file>
                </whitelist>
            </filter>
            
            <testsuites>
                <testsuite name="AllTests">
                    <directory>tests</directory>
                </testsuite>
            </testsuites>
            
            <logging>
                <log type="coverage-html"
                     target="results/phpunit/coverage-html"/>
                <log type="coverage-clover"
                     target="results/phpunit/coverage-clover.xml"/>
                <log type="junit"
                     target="results/phpunit/tests-junit.xml"
                     logIncompleteSkipped="false"/>
            </logging>
        </phpunit>

        ```
    + Suoritus
        + Jos asennettu järjestelmään:
            ```phpunit```
        + Jos asennettu Composerilla:
            ```vendor/bin/phpunit```
        + Jos valittaa "Coverage driver not found", asenna php xdebug
            
## Viikko 6

+ Translation Doctrinen Translatable behaviorilla
 ```
Databasen kääntämisestä:    https://github.com/KnpLabs/DoctrineBehaviors#translatable
                            http://symfony.com/doc/current/doctrine/common_extensions.html
    Siihen liittyvistä virheistä:
                            http://stackoverflow.com/questions/21058689/no-mapped-doctrine-orm-entities-according-to-the-current-configuration
                            http://stackoverflow.com/questions/15099060/doctrine2-class-is-not-a-valid-entity-or-mapped-super-class
                            http://stackoverflow.com/questions/18771107/how-to-configure-doctrine-file-mapping-driver-in-zf2
                            
                            "For whomever finds this question with similar problems try running:
                            php app/console doctrine:mapping:info
                            It will show you if you have problems in the mapping structure in doctrine"

Tässä translatorin toiminnasta ja käyttöönotosta:   http://symfony.com/doc/current/translation.html
Tässä localen toiminnasta:  http://symfony.com/doc/current/translation/locale.html 
```

## Viikko 8

+ Siirryttiin Symfonin käyttöön:
    https://symfony.com/doc/current/setup.html
+ Siirrä Cloud9 hostaamisen kansio (https://stackoverflow.com/questions/34236448/how-to-set-up-symfony2-on-cloud9):
    + Muokkaa kansiota tiedostoa /etc/apache2/sites-enabled/001-cloud9.conf
    + DocumentRoot -> home/ubuntu/workspace/{projektin nimi}/web

## Viikko 9

+ Symfony Namespace mapping päivitys:
    ```
    composer dumpautoload -o
    ```
    + Voit koittaa ilman -o
