<?php namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use AppBundle\Entity\Event;

class LoadEventData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $event1 = new Event();
        $event1->translate('fi')->setName('Arrival');
        $event1->translate('fi')->setDescription('Arrival on ohjaaja Denis Villeneuven (Sicario, Vangitut) ajatuksia herättävä science fiction -trilleri. Kun salaperäisiä avaruusaluksia laskeutuu ympäri maapalloa kielitieteilijä Louise Banksin (Amy Adams) johtama eliittitiimi kutsutaan tutkimaan.');
        $event1->translate('en')->setName('Arrival');
        $event1->translate('en')->setDescription('When mysterious spacecraft touch down across the globe, an elite team - lead by expert linguist Louise Banks - are brought together to investigate. As mankind teeters on the verge of global war, Banks and the team race against time for answers - and to find them, she will take a chance that could threaten her life, and quite possibly humanity.');
        $event1->setLinkToTrailer('https://youtu.be/3GSqOUTpfeI');
        $event1->addCategory($this->getReference('drama'));
        $event1->addCategory($this->getReference('thriller'));
        $event1->addCategory($this->getReference('sci-fi'));
        $manager->persist($event1);
        $event1->mergeNewTranslations();
        
        $event2 = new Event();
        $event2->translate('fi')->setName('La La Land');
        $event2->translate('fi')->setDescription('Here is for the fools who dream.\nLa La Land -elokuva kertoo aloittelevan näyttelijän Mian (Emma Stone) ja omistautuneen jazz-muusikon Sebastianin (Ryan Gosling) tarinan. He ponnistelevat unelmiensa eteen kaupungissa, joka tunnetusti tuhoaa toiveet ja särkee sydämet. Nykyhetken Los Angelesin päivittäisestä elämästä kertova alkuperäismusikaali tutustuttaa unelmien tavoittelun iloihin ja suruihin.\nOscar-®ehdokas Damien Chazellen (Whiplash) käsikirjoittama ja ohjaama La La Land on rakkaudenosoitus enkelten kaupungille ja Hollywood-musikaaleille.');
        $event2->translate('en')->setName('La La Land');
        $event2->translate('en')->setDescription('Mia, an aspiring actress, serves lattes to movie stars in between auditions and Sebastian, a jazz musician, scrapes by playing cocktail party gigs in dingy bars, but as success mounts they are faced with decisions that begin to fray the fragile fabric of their love affair, and the dreams they worked so hard to maintain in each other threaten to rip them apart.');
        $event2->setLinkToTrailer('https://www.youtube.com/watch?v=0pdqf4P9MB8');
        $event2->addCategory($this->getReference('drama'));
        $event2->addCategory($this->getReference('comedy'));
        $event2->addCategory($this->getReference('romance'));
        $event2->addCategory($this->getReference('musical'));
        $manager->persist($event2);
        $event2->mergeNewTranslations();
        
        $manager->flush();
    }
    
    public function getOrder() {
        return 2;
    }
}

?>
