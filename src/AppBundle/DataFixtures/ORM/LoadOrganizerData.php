
<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Organizer;

class LoadOrganizerData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $organizer = new Organizer();

        $manager->persist($organizer);
        $manager->flush();
    }
    
    public function getOrder() {
        // return 1;
    }
}

?>
