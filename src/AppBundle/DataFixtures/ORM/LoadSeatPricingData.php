
<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\SeatPricing;

class LoadSeatPricingData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $seatPricing = new SeatPricing();

        $manager->persist($seatPricing);
        $manager->flush();
    }
    
    public function getOrder() {
        // return 1;
    }
}

?>
