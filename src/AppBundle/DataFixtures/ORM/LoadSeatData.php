
<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Seat;

class LoadSeatData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $seat = new Seat();

        $manager->persist($seat);
        $manager->flush();
    }
    
    public function getOrder() {
        // return 1;
    }
}

?>
