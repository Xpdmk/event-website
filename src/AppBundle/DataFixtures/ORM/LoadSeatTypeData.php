
<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\SeatType;

class LoadSeatTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $seatType = new SeatType();

        $manager->persist($seatType);
        $manager->flush();
    }
    
    public function getOrder() {
        // return 1;
    }
}

?>
