<?php namespace AppBundle\Handler;

use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\JsonSerializationVisitor;
use JMS\Serializer\Context;

use AppBundle\Entity\User;

class UserSerializer implements SubscribingHandlerInterface {
    public static function getSubscribingMethods() {
        
        return array(
            array(
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format' => 'json',
                'type' => 'AppBundle\\Entity\\User',
                'method' => 'serializeUser',
            ),
        );
    }

    public function serializeUser(JsonSerializationVisitor $visitor, User $user, array $type, Context $context) {
        $array = [
            "username" => $user->getUsername()
            ];
        return $array;
    }
}