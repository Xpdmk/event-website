<?php namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="Space")
 **/
class Space {
    
    /** 
     * @ORM\Id 
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;
    
    /** @ORM\Column(type="string") **/
    protected $spaceName;
    
    /**
     * Many Space have One Location.
     * @ORM\ManyToOne(targetEntity="Location")
     * @ORM\JoinColumn(name="locationId", referencedColumnName="id")
     */
    protected $locationId;

    public function getId()
    {
        return $this->id;
    }

    public function getSpaceName()
    {
        return $this->spaceName;
    }
    public function setSpaceName($spaceName)
    {
        $this->spaceName = $spaceName;
    }

    public function getLocationId()
    {
        return $this->locationId;
    }
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;
    }

}
