<?php namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity 
 * @ORM\Table(name="PostalCode")
 **/
class PostalCode {
    
    /** 
     * @ORM\Column(name="code", type="string")
     * @ORM\Id
     */
    protected $postalCode;
    
    /** @ORM\Column(type="string") **/
    protected $city;

    public function getPostalCode()
    {
        return $this->postalCode;
    }
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
    }

    public function getCity()
    {
        return $this->city;
    }
    public function setCity($city)
    {
        $this->city = $city;
    }

}
