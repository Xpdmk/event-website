<?php namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="Event")
 */
class Event {
 
    use ORMBehaviors\Translatable\Translatable;
    
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /** @ORM\Column(type="string", nullable=true) **/
    protected $linkToTrailer;
    
    /**
     * Many Events have One EventType.
     * @ORM\ManyToOne(targetEntity="EventType", inversedBy="events")
     * @ORM\JoinColumn(name="eventTypes", referencedColumnName="id")
     */
    protected $eventTypes;
    /**
     * Many Events have Many Categories.
     * @ORM\ManyToMany(targetEntity="Category", mappedBy="events")
     * @ORM\JoinTable(name="eventsCategories")
     */
    protected $categories;
    
    /**
     * One Event has Many Presentations.
     * @ORM\OneToMany(targetEntity="Presentation", mappedBy="eventId")
     */
    protected $presentations;
    
    public function __construct()
    {
        $this->presentations = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->eventTypes = new ArrayCollection();
        $this->reviews = new ArrayCollection();
        }
    /**
     * One Event has Many Reviews.
     * @ORM\OneToMany(targetEntity="Review", mappedBy="eventId")
     */
    protected $reviews;
    
    /**
     * Constructor
     */
 public function addPresentation(Presentation $presentation)
    {
        $this->presentations->add($presentation);
    }

    public function removePresentation(Presentation $presentation)
    {
        $this->presentations->removeElement($presentation);
    }
     public function addEventType(EventType $presentation)
    {
        $this->eventTypes->add($presentation);
    }

    public function removeEventType(EventType $presentation)
    {
        $this->eventTypes->removeElement($presentation);
    }
    public function getId(){
        return $this->id;
    }
    
    public function getLinkToTrailer(){
        return $this->linkToTrailer;
    }
    public function setLinkToTrailer($linkToTrailer){
        $this->linkToTrailer = $linkToTrailer;
    }

    
    public function getPresentations() {
        return $this->presentations;
    }
    public function setPresentations($presentations) {
        $this->presentations = $presentations;
    }
    
    public function getReviews() {
        return $this->reviews;
    }
    public function setReviews($reviews) {
        $this->reviews = $reviews;
    }
    
    public function getEventTypes() {
        return $this->eventTypes;
    }
    public function setEventTypes($eventTypes) {
        $this->eventTypes = $eventTypes;
    }
    
    public function getCategories() {
        return $this->categories;
    }
    public function setCategories($categories) {
        $this->categories = $categories;
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => Event::class,
        ));
    }

    /**
     * Add category
     *
     * @param \AppBundle\Entity\Category $category
     *
     * @return Event
     */
    public function addCategory(\AppBundle\Entity\Category $category)
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addEvent($this);            
        }
        
        return $this;
    }

    /**
     * Remove category
     *
     * @param \AppBundle\Entity\Category $category
     */
    public function removeCategory(\AppBundle\Entity\Category $category)
    {
        $this->categories->removeElement($category);
    }

 


    /**
     * Add review
     *
     * @param \AppBundle\Entity\Review $review
     *
     * @return Event
     */
    public function addReview(\AppBundle\Entity\Review $review)
    {
        $this->reviews[] = $review;

        return $this;
    }

    /**
     * Remove review
     *
     * @param \AppBundle\Entity\Review $review
     */
    public function removeReview(\AppBundle\Entity\Review $review)
    {
        $this->reviews->removeElement($review);
    }
}

