<?php namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="EventTypeTranslation")
 **/
class EventTypeTranslation {
    use ORMBehaviors\Translatable\Translation;
    
    /** 
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    protected $name;
    
     public function getName() {
        return $this->name;
    }
    public function setType($name) {
        $this->name = $name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return EventTypeTranslation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}
