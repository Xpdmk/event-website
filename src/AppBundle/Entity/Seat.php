<?php namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="Seat")
 **/
class Seat {
    
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;
    
    /**
     * Many Seat have One SeatType.
     * @ORM\ManyToOne(targetEntity="SeatType")
     * @ORM\JoinColumn(name="seatType", referencedColumnName="id")
     */
    protected $seatTypeId;
    
    /**
     * Many Seat have One Space.
     * @ORM\ManyToOne(targetEntity="Space")
     * @ORM\JoinColumn(name="spaceId", referencedColumnName="id")
     */
    protected $spaceId;
    
    /** @ORM\Column(type="integer") */
    protected $seatRow;
    
    /** @ORM\Column(type="integer") */
    protected $seatNumber;

    public function getId()
    {
        return $this->id;
    }

    public function getSeatTypeId()
    {
        return $this->seatTypeId;
    }
    public function setSeatTypeId($seatTypeId)
    {
        $this->seatTypeId = $seatTypeId;
    }

    public function getSpaceId()
    {
        return $this->spaceId;
    }
    public function setSpaceId($spaceId)
    {
        $this->spaceId = $spaceId;
    }

    public function getSeatRow()
    {
        return $this->seatRow;
    }
    public function setSeatRow($seatRow)
    {
        $this->seatRow = $seatRow;
    }

    public function getSeatNumber()
    {
        return $this->seatNumber;
    }
    public function setSeatNumber($seatNumber)
    {
        $this->seatNumber = $seatNumber;
    }

}
