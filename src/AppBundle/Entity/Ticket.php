<?php namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="Ticket")
 **/
class Ticket {
    
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue 
     */
    protected $id;
    
    /**
     * Many Tickets have One Customer.
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="tickets")
     * @ORM\JoinColumn(name="customerId", referencedColumnName="id")
     */
    protected $customerId;
    
    /**
     * Many Tickets have One Seat pricing.
     * @ORM\ManyToOne(targetEntity="SeatPricing")
     * @ORM\JoinColumn(name="seatPricing", referencedColumnName="id")
     */
    protected $seatPricing;
    
    /**
     * Many Tickets have One Seat.
     * @ORM\ManyToOne(targetEntity="Seat")
     * @ORM\JoinColumn(name="seat", referencedColumnName="id")
     */
    protected $seat;

    public function getId()
    {
        return $this->id;
    }

    public function getCustomerId()
    {
        return $this->customerId;
    }
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    public function getSeatPricing()
    {
        return $this->seatPricing;
    }
    public function setSeatPricing($seatPricing)
    {
        $this->seatPricing = $seatPricing;
    }

    public function getSeat()
    {
        return $this->seat;
    }
    public function setSeat($seat)
    {
        $this->seat = $seat;
    }
}
