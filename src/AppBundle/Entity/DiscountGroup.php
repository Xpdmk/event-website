<?php namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="DiscountGroup")
 */
class DiscountGroup {
    
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    protected $name;
    
    public function getName()
    {
        return $this->discountGroupName;
    }
    public function setName($discountGroupName)
    {
        $this->discountGroupName = $discountGroupName;
    }

    public function getId()
    {
        return $this->id;
    }

}
