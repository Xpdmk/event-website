<?php namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity 
 * @ORM\Table(name="Review")
 **/
class Review {
    
    /**
     * @var int
     * @ORM\Id 
     * @ORM\Column(type="integer") 
     * @ORM\GeneratedValue 
     */
    protected $id;
    
    /**
     * Many Presentations have One Event.
     * @ORM\ManyToOne(targetEntity="Event", inversedBy="reviews")
     */
    protected $eventId;
    
    /**
     * Many Reviews have One Customer.
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="reviews")
     * @ORM\JoinColumn(name="customerId", referencedColumnName="id")
     */
    protected $customerId;
    
    /** @ORM\Column(type="integer") */
    protected $stars;
    
    public function getId(){
        return $this->id;
    }
    
    public function getEventId(){
        return $this->eventId;
    }
    public function setEventId($eventId){
        $this->eventId = $eventId;
    }
    
    public function getCustomerId(){
        return $this->customerId;
    }
    public function setCustomerId($customerId){
        $this->customerId = $customerId;
    }
    
    public function getStars(){
        return $this->stars;
    }
    public function setStars($stars){
        $this->stars = $stars;
    }

}
