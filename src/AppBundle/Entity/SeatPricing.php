<?php namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity 
 * @ORM\Table(name="SeatPricing")
 */
class SeatPricing {
    
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;
    
    /** @ORM\Column(type="float") */
    protected $price;
    
    /** @ORM\Column(type="integer") */
    protected $max;
    
    /**
     * Many SeatPricings have One DiscountGroup.
     * @ORM\ManyToOne(targetEntity="DiscountGroup")
     * @ORM\JoinColumn(name="discountGroupId", referencedColumnName="id")
     */
    protected $discountGroupId;
    
    /**
     * Many SeatPricings have One Presentation.
     * @ORM\ManyToOne(targetEntity="Presentation")
     * @ORM\JoinColumn(name="presentationId", referencedColumnName="id")
     */
    protected $presentationId;
    
    /**
     * Many SeatPricings have One SeatType.
     * @ORM\ManyToOne(targetEntity="SeatType")
     * @ORM\JoinColumn(name="seatTypeId", referencedColumnName="id")
     */
    protected $seatTypeId;

    public function getId()
    {
        return $this->id;
    }

    public function getPrice()
    {
        return $this->price;
    }
    public function setPrice($price)
    {
        $this->price = $price;
    }
    
    public function getMax()
    {
        return $this->max;
    }
    public function setMax($max)
    {
        $this->max = $max;
    }

    public function getDiscountGroupId()
    {
        return $this->discountGroupId;
    }
    public function setDiscountGroupId($discountGroupId)
    {
        $this->discountGroupId = $discountGroupId;
    }

    public function getPresentationId()
    {
        return $this->presentationId;
    }
    public function setPresentationId($presentationId)
    {
        $this->presentationId = $presentationId;
    }

    public function getSeatTypeId()
    {
        return $this->seatTypeId;
    }
    public function setSeatTypeId($seatTypeId)
    {
        $this->seatTypeId = $seatTypeId;
    }

}
