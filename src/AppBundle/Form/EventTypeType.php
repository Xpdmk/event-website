<?php
namespace AppBundle\Form;

use AppBundle\Entity\EventType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use A2lix\TranslationFormBundle\Form\Type\TranslationsType;




class EventTypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('translations', TranslationsType::class, [
	    'fields' => [ 
	        'name', ChoiceType::class, array(
		    'choices' => array(
		        '' => '',
		        'no' => false,
		        'maybe' => null,
		    ),
			    'label' => $options['label']['eventtypes'])
	    ],
	]);

    }
    

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => EventType::class,
        ));
    }
}

?>