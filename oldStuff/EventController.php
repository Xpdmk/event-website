<?php
// Load dependences
require_once(__DIR__."/config/dependenceLoader.php");
loadDependences(__FILE__);

/**
 * EventController
 *
 * @package src
 * @author `Otso, Roy, Tapio, Lauri`
 */
class EventController {
    private $dataManager;
    private $htmlBuilder;
    private $dataArray; // global array for data
    // convert data from disk to a variable:
    private $recoveredData;
    // unserializing to get actual array
 

    /**
 * A constructor that initializes model.
 */
    public function __construct()
    {
        $config = json_decode(file_get_contents("config/websiteConfig.json"), true);
        $this->dataManager =  new $config["DataManager"]();
        $this->htmlBuilder = new $config["HTMLBuilder"]();
    }
    
/**
 * Initializes REST's dataArray and the View.
 */
    public function init()
    {
        //$this ->dataArray = array();
        //$this ->recoveredData = file_get_contents('restdata.txt');
        //$this ->dataArray= unserialize($recoveredData);
        $json = $this->dataManager->getNextEvents(10);
        echo $this->htmlBuilder->createMainPage($json);
        
        
    }
        // initialize
    

    # URI parser helper functions
    # ---------------------------
    /**
     * Parses the URI to an array.
     * @return array
     **/
    public function getResource() {
        # returns numerically indexed array of URI parts
        $resource_string = $_SERVER['REQUEST_URI'];
        if (strstr($resource_string, '?')) {
            $resource_string = substr($resource_string, 0, strpos($resource_string, '?'));
        }
        $resource = array();
        $resource = explode('/', $resource_string);
        array_shift($resource);   
        return $resource;
    }


    /**
     * Parses the parameters from URI.
     * @return array
     **/
    public function getParameters() {
        # returns an associative array containing the parameters
        $resource = $_SERVER['REQUEST_URI'];
        $param_string = "";
        $param_array = array();
        if (strstr($resource, '?')) {
            # URI has parameters
            $param_string = substr($resource, strpos($resource, '?')+1);
            $parameters = explode('&', $param_string);                      
            foreach ($parameters as $single_parameter) {
                $param_name = substr($single_parameter, 0, strpos($single_parameter, '='));
                $param_value = substr($single_parameter, strpos($single_parameter, '=')+1);
                $param_array[$param_name] = $param_value;
            }
        }
        return $param_array;
        
    }

    /**
     * Returns the method the incoming request is using.
     * @return string
     **/
    public function getMethod() {
        # returns a string containing the HTTP method
        $method = $_SERVER['REQUEST_METHOD'];
        return $method;
    }
    
    public function handleResponse() {
        $resource = $this->getResource();
        $request_method = $this->getMethod();
        $parameters = $this->getParameters();
        
        # Redirect to appropriate functions
        if ($resource[0]=="event-website") {
            # Ignore
            if (sizeof($resource) == 3 && $resource[2] == "Starter.php") {
                return;
            }
            
            # HTML
            else if ($resource[1] == "event" && sizeof($resource)==3) {
                $event = $this->dataManager->getEvent($resource[2]);
                echo $this->htmlBuilder->createEventPage($event);
        		exit;
            }
            
        	# JSON
        	else if ($resource[1] == "events") {
        		echo $this->dataManager->getEventsOld();
        		exit;
        	} else if ($resource[1] == "allEventsWithObjects") {
        	    echo $this->dataManager->getEvents();
        	    exit;
        	} else if (sizeof($resource) > 3 && $resource[3] == "10Events") {
        		echo $this->dataManager->getNextEvents(10);
        		exit;
        	} else if (sizeof($resource) == 4 && $resource[3] == "searchResources") {
                echo $this->dataManager->getSearchResources();
                exit;
        	} else if (sizeof($resource) == 4 && $resource[3]=="searchEvents" && sizeof($parameters)!=0){
        	    echo $this->dataManager->getSearchEvents($parameters["q"]);
        	    exit;
        	
        	# Images
            } else if (sizeof($resource) == 4 && $resource[3]=="carouselImages") {
        	    echo $controller->get_carouselImages($resource[4]);
        	    exit;
            }
        	// 	if ($request_method=="POST" && $resource[1]=="event") {
        //     	postPerson($parameters);
        // 	}	
        // 	else if ($request_method=="DELETE" && $resource[1]=="person") {
        // 		deletePerson($resource[2]);
        // 	}
        
            # Errors
        	else {
        		http_response_code(405); # Method not allowed
        	}
            //NOTE TO SELF: https://tapahtumasivusto-xpdmk.c9users.io/event-website/src/EventController.php/event/1 EIKÄ eventpage.php. CONTROLLER INCLUDEE PAGEN
        }
    }
    
    # Handler methods
    # ---------------
     
    /**
     * Uses the Model to get ten newest events from the database.
     * Returns JSON string.
     * @return string
     **/
    public function get_10Newest(){
        return $this->model->get_10Newest();
    }
    
    /**
     * Uses the Model to get an event by its ID from the database.
     * Returns JSON string.
     * @return string
     **/
    public function get_eventById($id) {
         
        include("eventpage.php");
        $json=$this->model->get_eventById($id);
        return "<script>eventPageLoaded('".$json."')</script>";
    }
    
    /**
     * Uses the Model to get all event names from the database.
     * Returns JSON string.
     * @return string
     **/
    public function get_eventNames() {
        return $this->model->get_eventNames();
    }
    
    /**
     * Uses the Model to get all events from the database.
     * Returns JSON string.
     * @return string
     **/
    public function get_allEvents(){
       return $this->model->get_allEvents();
    }
    
    /**
     * Uses the Model to get all events from the database, but using doctrine instead of SQL.
     * Returns JSON string.
     * @return string
     **/
    public function get_allEventsWithObjects() {
        return $this->model->get_allEventsWithObjects();
    }
    
    /**
     * Uses the Model to get all events' names and IDs from the database.
     * Returns JSON string.
     * @return string
     **/
    public function get_searchResources() {
        return $this->model->get_searchResources();
    }
    
    /**
     * Uses the Model to get a certain event's Image paths from the database.
     * Returns JSON string.
     * @return string
     **/
    public function get_carouselImages($id){
        return $this->model->get_carouselImages($id);
    }
    
    /**
     * Uses the Model to get a searched events' details from the database.
     * Returns JSON string.
     * @return string
     * @idArray : Array that has the ids.
     **/
    public function get_searchEvents($idArray){
        return $this->model->get_eventById($idArray);
    }
    
    //  /**
//  * Uses the Model to get all events from the database.
//  * Returns JSON string.
//  * @return string
//  **/
//     public function get_eventList()
//     {
//       $result= $this->model->get_eventList();
//       return $result; 
//     }
}

$controller = new EventController();
$controller->handleResponse();


?>