<?php
require_once(__DIR__."/config/dependenceLoader.php");
loadDependences(__FILE__);

class Navbar {
    private $navbar;
    
    
    public function __construct($json) {

        $navbar = new Template("html/navbar.tpl");
        options();
    }
    
    public function __toString() {
        return $this->navbar;
    }
    
    /**
     * Fills navbar's annotations.
     */
    public function options(){
        $navbar->set("loginState",new Template("html/navLoginStateFalse.tpl"));
    }
}

class loggedInNavbar extends Navbar{
    
    /**
     * Overrwrites Navbar's options -method and fills navbar's annotations.
     */
    public function options(){
        $navbar->set("loginState",new Template("html/navLoginStateTrue.tpl"));
    }
}

?>