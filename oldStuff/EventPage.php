<?php
// Load dependences
require_once(__DIR__."/config/dependenceLoader.php");
loadDependences(__FILE__);

class EventPage extends Page {
    
    /**
     * Generates event page HTML code using associative array of an event
     */
    public function __construct($json) {
        $properties = loadProperties("default", __FILE__);
    
        // Create HTML properties tags
        $beginning = new Template("html/documentProperties.tpl");
        $beginning->set("lang", "en"); // TODO: Toteuta haku jsonista
        
        // Add head data to head
        $head = new HTMLElement("head");
        $head->setInnerHTML($properties);
        
        $container = new HTMLElement("div");
        $container->setAttribute("class", "container");
        
        $navbar = new Template("html/navbar.tpl");
        
        $eventPage = new Template("html/eventPage.tpl");
        /* TODO: Lisää:
        * - title
        * - rating
        * - carousel
        * - description
        * - presentation body
        */
        
        $bodyContent = $navbar . $eventPage;
        
        $body = new HTMLElement("body");
        $body->setInnerHTML($bodyContent);
        
        $this->str = $beginning . $head . $body;
    }
    
    
    /**
     * Returns generated string of HTML code of the site
     * @return : String
     */
    public function __toString() {
        return $this->str;
    }
}

?>