<?php namespace AppBundle\Model;

interface Converter {
    public function convert($thing);
}