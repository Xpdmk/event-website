<?php  namespace AppBundle\Entities;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 * @ORM\Table(name="Event")
 */
class Event {
    use ORMBehaviors\Translatable\Translatable;
    
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
   
    /**
     * Many Events have One EventType.
     * @ManyToOne(targetEntity="EventTypeTranslation", inversedBy="events")
     * @JoinColumn(name="eventType", referencedColumnName="type")
     */
    protected $eventType;
    /**
     * Many Events have Many Categories.
     * @ManyToMany(targetEntity="Category", mappedBy="events")
     * @JoinTable(name="eventsCategories")
     */
    protected $categories;

    /**
     * @var string
     **/
    /** @Column(type="string", nullable=true) **/
    protected $linkToTrailer;
    
    /**
     * One Event has Many Presentations.
     * @OneToMany(targetEntity="Presentation", mappedBy="eventId")
     */
    protected $presentations;
    
    /**
     * One Event has Many Reviews.
     * @OneToMany(targetEntity="Review", mappedBy="eventId")
     */
    protected $reviews;
    
    public function getId(){
        return $this->id;
    }
    
    public function getLinkToTrailer(){
        return $this->linkToTrailer;
    }
    public function setLinkToTrailer($linkToTrailer){
        $this->linkToTrailer = $linkToTrailer;
    }
    
    public function getPresentations() {
        return $this->presentations;
    }
    public function setPresentations($presentations) {
        $this->presentations = $presentations;
    }
    
    public function getReviews() {
        return $this->reviews;
    }
    public function setReviews($reviews) {
        $this->reviews = $reviews;
    }
    
    public function getEventType() {
        return $this->eventType;
    }
    public function setEventType($eventType) {
        $this->eventType = $eventType;
    }
    
    public function getCategories() {
        return $this->categories;
    }
    public function setCategories($categories) {
        $this->categories = $categories;
    }
}

/**
 * @Entity @Table(name="Presentation")
 **/
class Presentation {
    
    /**
     * @var int
     */
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /**
     * @var date
     */
    /** @Column(type="date") **/
    protected $startDate;
    /**
     * @var date
     */
    /** @Column(type="date") **/
    protected $endDate;
    /**
     * @var time
     */
    /** @Column(type="time") **/
    protected $startTime;
    /**
     * @var time
     */
    /** @Column(type="time") **/
    protected $endTime;
    /**
     * Many Presentations have One Event.
     * @ManyToOne(targetEntity="Event", inversedBy="presentations")
     * @JoinColumn(name="eventId", referencedColumnName="id")
     */
    protected $eventId;
    /**
     * Many Presentations have One Organizer.
     * @ManyToOne(targetEntity="Organizer")
     * @JoinColumn(name="organizerId", referencedColumnName="id")
     */
    protected $organizerId;
    /**
     * Many Presentations have One Location.
     * @ManyToOne(targetEntity="Location")
     * @JoinColumn(name="locationId", referencedColumnName="id")
     */
    protected $locationId;

    public function getId(){
        return $this->id;
    }
    
    public function getStartDate(){
        return $this->startDate;
    }
    public function setStartDate($startDate){
        $this->startDate = $startDate;
    }
    
    public function getEndDate(){
        return $this->endDate;
    }
    public function setEndDate($endDate){
        $this->endDate = $endDate;
    }
    
    public function getStartTime(){
        return $this->startTime;
    }
    public function setStartTime($startTime){
        $this->startTime = $startTime;
    }
    
    public function getEndTime(){
        return $this->endTime;
    }
    public function setEndTime($endTime){
        $this->endTime = $endTime;
    }
    
    public function getEventId() {
        return $this->eventId;
    }
    public function setEventId($eventId) {
        $this->eventId = $eventId;
    }

    public function getOrganizerId()
    {
        return $this->organizerId;
    }
    public function setOrganizerId($organizerId)
    {
        $this->organizerId = $organizerId;
    }

    public function getLocationId()
    {
        return $this->locationId;
    }
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;
    }


}
/**
 * @Entity @Table(name="Review")
 **/
class Review {
    /**
     * @var int
     */
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /**
     * Many Presentations have One Event.
     * @ManyToOne(targetEntity="Event", inversedBy="reviews")
     * @JoinColumn(name="eventId", referencedColumnName="id")
     */
    protected $eventId;
    /**
     * Many Reviews have One Customer.
     * @ManyToOne(targetEntity="Customer", inversedBy="reviews")
     * @JoinColumn(name="customerId", referencedColumnName="id")
     */
    protected $customerId;
    /**
     * @var integer
     */
    /** @Column(type="integer") **/
    protected $stars;
    
    public function getId(){
        return $this->id;
    }
    
    public function getEventId(){
        return $this->eventId;
    }
    public function setEventId($eventId){
        $this->eventId = $eventId;
    }
    
    public function getCustomerId(){
        return $this->customerId;
    }
    public function setCustomerId($customerId){
        $this->customerId = $customerId;
    }
    
    public function getStars(){
        return $this->stars;
    }
    public function setStars($stars){
        $this->stars = $stars;
    }

}
/**
 * @Entity @Table(name="Customer")
 **/
class Customer {
    /**
     * @var int
     */
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /**
     * @var string
     */
    /** @Column(type="string") **/
    protected $firstName;
    /**
     * @var string
     */
    /** @Column(type="string") **/
    protected $lastName;
     /**
     * @var string
     */
    /** @Column(type="string") **/
    protected $password;

    /**
     * One Customer has Many Reviews.
     * @OneToMany(targetEntity="Review", mappedBy="id")
     */
    protected $reviews;
        /**
     * One Customer has Many Tickets.
     * @OneToMany(targetEntity="Ticket", mappedBy="id")
     */
    protected $tickets;
    /**
     * @var string
     */
    /** @Column(type="string") **/
    protected $email;
       public function getPassword(){
        return $this->password;
    }
    public function setPassword($password){
        $this->password=$password;
    }
    public function getId(){
        return $this->id;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    public function getEmail()
    {
        return $this->email;
    }
    public function setEmail($email)
    {
        $this->email = $email;
    }
    public function getTickets(){
        return $this->tickets;
    }
    public function getReviews(){
        return $this->reviews;
    }
    public function setReviews($reviews){
        $this->reviews=$reviews;
    }
    public function setTickets($tickets){
        $this->tickets=$tickets;
    }
}

/**
 * @Entity @Table(name="Ticket")
 **/
class Ticket {
    /**
     * @var int
     */
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /**
     * Many Tickets have One Customer.
     * @ManyToOne(targetEntity="Customer")
     * @JoinColumn(name="customerId", referencedColumnName="id")
     */
    protected $customerId;
    /**
     * Many Tickets have One Seat pricing.
     * @ManyToOne(targetEntity="SeatPricing")
     * @JoinColumn(name="seatPricing", referencedColumnName="id")
     */
    protected $seatPricing;
    /**
     * Many Tickets have One Seat.
     * @ManyToOne(targetEntity="Seat")
     * @JoinColumn(name="seat", referencedColumnName="id")
     */
    protected $seat;

    public function getId()
    {
        return $this->id;
    }

    public function getCustomerId()
    {
        return $this->customerId;
    }
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    public function getSeatPricing()
    {
        return $this->seatPricing;
    }
    public function setSeatPricing($seatPricing)
    {
        $this->seatPricing = $seatPricing;
    }

    public function getSeat()
    {
        return $this->seat;
    }
    public function setSeat($seat)
    {
        $this->seat = $seat;
    }
}

/**
 * @Entity @Table(name="Seat")
 **/
class Seat {
    /**
     * @var int
     */
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /**
     * Many Seat have One SeatType.
     * @ManyToOne(targetEntity="SeatType")
     * @JoinColumn(name="seatType", referencedColumnName="id")
     */
    protected $seatTypeId;
    /**
     * Many Seat have One Space.
     * @ManyToOne(targetEntity="Space")
     * @JoinColumn(name="spaceId", referencedColumnName="id")
     */
    protected $spaceId;
    /**
     * @var integer
     */
    /** @Column(type="integer") **/
    protected $seatRow;
    /**
     * @var integer
     */
    /** @Column(type="integer") **/
    protected $seatNumber;

    public function getId()
    {
        return $this->id;
    }

    public function getSeatTypeId()
    {
        return $this->seatTypeId;
    }
    public function setSeatTypeId($seatTypeId)
    {
        $this->seatTypeId = $seatTypeId;
    }

    public function getSpaceId()
    {
        return $this->spaceId;
    }
    public function setSpaceId($spaceId)
    {
        $this->spaceId = $spaceId;
    }

    public function getSeatRow()
    {
        return $this->seatRow;
    }
    public function setSeatRow($seatRow)
    {
        $this->seatRow = $seatRow;
    }

    public function getSeatNumber()
    {
        return $this->seatNumber;
    }
    public function setSeatNumber($seatNumber)
    {
        $this->seatNumber = $seatNumber;
    }

}

/**
 * @ORM\Entity
 * @ORM\Table(name="SeatType")
 */
class SeatType {
	use ORMBehaviors\Translatable\Translatable;
	
/**
 * @var integer
 *
 * @ORM\Column(name="id", type="integer")
 * @ORM\Id
 * @ORM\GeneratedValue(strategy="AUTO")
 * 
 */
    protected $id;
   

    public function getId()
    {
        return $this->id;
    }

}
/**
 * @ORM\Entity
 **/
class SeatTypeTranslation{
	use ORMBehaviors\Translatable\Translation;
    
    /**
     * @ORM\Column(type="string")
     * @Assert\NotEmpty
     * @var string
     */
    protected $seatTypeName;
    
    public function getSeatTypeName()
    {
        return $this->seatTypeName;
    }
    public function setSeatTypeName($seatTypeName)
    {
        $this->seatTypeName = $seatTypeName;
    }
    
   
}



/**
 * @Entity @Table(name="SeatPricing")
 **/
class SeatPricing {
    /**
     * @var int
     */
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /**
     * @var double
     */
    /** @Column(type="float") **/
    protected $price;
    /**
     * @var integer
     */
    /** @Column(type="integer") **/
    protected $max;
    /**
     * Many SeatPricings have One DiscountGroup.
     * @ManyToOne(targetEntity="DiscountGroup")
     * @JoinColumn(name="discountGroupId", referencedColumnName="id")
     */
    protected $discountGroupId;
    /**
     * Many SeatPricings have One Presentation.
     * @ManyToOne(targetEntity="Presentation")
     * @JoinColumn(name="presentationId", referencedColumnName="id")
     */
    protected $presentationId;
    /**
     * Many SeatPricings have One SeatType.
     * @ManyToOne(targetEntity="SeatType")
     * @JoinColumn(name="seatTypeId", referencedColumnName="id")
     */
    protected $seatTypeId;

    public function getId()
    {
        return $this->id;
    }

    public function getPrice()
    {
        return $this->price;
    }
    public function setPrice($price)
    {
        $this->price = $price;
    }
    
    public function getMax()
    {
        return $this->max;
    }
    public function setMax($max)
    {
        $this->max = $max;
    }

    public function getDiscountGroupId()
    {
        return $this->discountGroupId;
    }
    public function setDiscountGroupId($discountGroupId)
    {
        $this->discountGroupId = $discountGroupId;
    }

    public function getPresentationId()
    {
        return $this->presentationId;
    }
    public function setPresentationId($presentationId)
    {
        $this->presentationId = $presentationId;
    }

    public function getSeatTypeId()
    {
        return $this->seatTypeId;
    }
    public function setSeatTypeId($seatTypeId)
    {
        $this->seatTypeId = $seatTypeId;
    }

}

/**
 * @Entity @Table(name="Space")
 **/
class Space {
    /**
     * @var int
     */
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /**
     * @var string
     */
    /** @Column(type="string") **/
    protected $spaceName;
    /**
     * Many Space have One Location.
     * @ManyToOne(targetEntity="Location")
     * @JoinColumn(name="locationId", referencedColumnName="id")
     */
    protected $locationId;

    public function getId()
    {
        return $this->id;
    }

    public function getSpaceName()
    {
        return $this->spaceName;
    }
    public function setSpaceName($spaceName)
    {
        $this->spaceName = $spaceName;
    }

    public function getLocationId()
    {
        return $this->locationId;
    }
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;
    }

}

/**
 * @Entity @Table(name="Location")
 **/
class Location {
    /**
     * @var int
     */
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /**
     * @var string
     */
    /** @Column(type="string") **/
    protected $streetAddress;
    /**
     * Many Location have One PostalCode.
     * @ManyToOne(targetEntity="PostalCode")
     * @JoinColumn(name="postalCode", referencedColumnName="postalCode")
     */
    protected $postalCode;

    public function getId()
    {
        return $this->id;
    }

    public function getStreetAddress()
    {
        return $this->streetAddress;
    }
    public function setStreetAddress($streetAddress)
    {
        $this->streetAddress = $streetAddress;
    }

    public function getPostalCode()
    {
        return $this->postalCode;
    }
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
    }
}

/**
 * @Entity @Table(name="PostalCode")
 **/
class PostalCode {
    /**
     * @var string
     */
    /** @Id @Column(type="string") **/
    protected $postalCode;
    /**
     * @var string
     */
    /** @Column(type="string") **/
    protected $city;

    public function getPostalCode()
    {
        return $this->postalCode;
    }
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
    }

    public function getCity()
    {
        return $this->city;
    }
    public function setCity($city)
    {
        $this->city = $city;
    }

}

/**
 * @ORM\Entity
 * @ORM\Table(name="DiscountGroup")
 */
class DiscountGroup {
    	use ORMBehaviors\Translatable\Translatable;
  /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     */
    protected $id;
   

    public function getId()
    {
        return $this->id;
    }

}

/**
 * @ORM\Entity
 */
class DiscountGroupTranslation{
	
	use ORMBehaviors\Translatable\Translation;


    
      /**
     * @ORM\Column(type="string")
     * @Assert\NotEmpty
     * @var string
     */
    protected $name;
    
    public function getName()
    {
        return $this->discountGroupName;
    }
    public function setName($discountGroupName)
    {
        $this->discountGroupName = $discountGroupName;
    }
    
   
}

/**
 * @Entity @Table(name="Organizer")
 **/
class Organizer {
    /**
     * @var int
     */
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
     /**
     * @ORM\Column(type="string")
     * @Assert\NotEmpty
     * @var string
     */
    protected $organizerName;

    public function getId()
    {
        return $this->id;
    }

    public function getOrganizerName()
    {
        return $this->organizerName;
    }
    public function setOrganizerName($organizerName)
    {
        $this->organizerName = $organizerName;
    }

}

/**
 * @ORM\Entity
 * @ORM\Table(name="Category")
 */
class Category {
    use ORMBehaviors\Translatable\Translatable;

  /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     */
    protected $id; // Tarpeellinen ManyToMany suhteiden säilyttämiseksi Event:in ja Category:n välillä

    /**
     * Many Categories have Many Events.
     * @ManyToMany(targetEntity="Event", inversedBy="categories")
     */
    protected $events;
   
    
    public function getEvents() {
        return $this->events;
    }
    public function setEvents($events) {
        $this->events = $events;
    }
}
/**
 * @ORM\Entity
 */
class CategoryTranslation{
	use ORMBehaviors\Translatable\Translation;
      /**
     * @ORM\Column(type="string")
     * @Assert\NotEmpty
     * @var string
     */
    protected $name;
    
    public function getName() {
        return $this->name;
    }
    public function setName($name) {
        $this->name = $name;
    }
}

/**
 * @Entity @Table(name="EventType")
 **/
class EventType {
    
    use ORMBehaviors\Translatable\Translatable;

    /**
     * @var int
     */
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /**
     * One EventType has Many Events.
     * @OneToMany(targetEntity="Event", mappedBy="id")
     */
    protected $events;
    

    
    public function getEvents() {
        return $this->events;
    }
    public function setEvents($events) {
        $this->events = $events;
    }
}

/**
 * @Entity 
 **/
class EventTypeTranslation{
    	
    use ORMBehaviors\Translatable\Translation;
	
	/**
     * @var string
     */
    /** @Id @Column(type="string") **/
    protected $type;

    
    public function getType() {
        return $this->type;
    }
    public function setType($type) {
        $this->type = $type;
    }
}
?>