<?php
/**
 * HTMLElement is a class for creating HTML elements with ease.
 */
class HTMLElement {
    protected $tag;
    protected $attributes;
    protected $innerHTML;
    
    /**
     * 
     * @tag : String without whitespaces
     * @attributes : Associative array
     * @innerHTML : String or array of mix of strings and HTMLElements
     */
    public function __construct($tag, $attributes = array(), $innerHTML = null) {
        // Check if $tag is a string and it has no whitespaces
        if (gettype($tag) == "string" && !preg_match('/\s/',$tag)) { // Is string and has no whitespaces
            $this->tag = $tag;
        } else {
            throw new RuntimeException("Tag has to be a string and contains no whitespaces");
        }
        
        // Check attributes
        if (gettype($attributes) == "array") {
            $whitespaceKey = $this->checkKeyWhitespaces($attributes);
            if ($whitespaceKey) {
                throw new RuntimeException("Attributes can't have whitespaces. Attribute '".$attribute."' has whitespaces.");
            }
            $this->attributes = $attributes;
        } else {
            throw new RuntimeException("Attributes must be array. ".gettype($attributes)." given.");
        }
        
        // Check innerHTML variable
        switch (gettype($innerHTML)) {
            case "string":
                $this->innerHTML = [$innerHTML];
                break;
            case "object":
                if ($this->isClassOfThis($innerHTML)) {
                    $this->innerHTML = [$innerHTML];
                } else {
                    throw new RuntimeException("InnerHTML was object, but it's class is not HTMLElement.");
                }
                break;
            case "array":
                $foreach($innerHTML as $t) {
                    if (gettype($t) != "string" || !$this->isClassOfThis($t)) {
                        throw new RuntimeException("InnerHTML contains object that is not HTMLElement or value that is not string.");
                    }
                }
                $this->innerHTML = $innerHTML;
                break;
            default:
                throw new RuntimeException("InnerHTML was not array of mix oor HTMLElement object");
        }
    }
    
    private function isClassOfThis($object) {
        return is_a(get_class($object), static::class);
    }
    
    private function checkKeyWhitespaces($array) {
        foreach ($attributes as $attribute => $value) {
            if (preg_match('/\s/',$attribute)) {
                return $attribute;
            }
        }
        return false;
    }
    
    /**
     * Sets an attribute to be added to HTML element when exported as string
     * @attribute : String name of the attribute to be set
     * @value : String value for attribute
     */
    public function setAttribute($attribute, $value) {
        // Check if parameters are strings
        $a = gettype($attribute) == "string"? true:false;
        $v = gettype($value) == "string"? true:false;
        if ($a) {
            if ($v) {
                $this->attributes[$attribute] = $value;
            } else {
                throw new RuntimeException("Value must be string. Type ".gettype($value)." given.");
            }
        } else {
            throw new RuntimeException("Attribute must be string. Type ".gettype($attribute)." given.");
        }
    }
    
    /**
     * Sets string value between beginning tag and ending tag.
     * For example:
     * <a>[here]</a>
     * 
     * @value : String or object that can be turned into string
     */
    public function setInnerHTML($value) {
        // Check if string or can be turned into string
        if (gettype($value) == "string") {
            $this->innerHTML = $value;
        } else if (method_exists($value, "__toString")) {
            $this->innerHTML = $value->__toString($value);
        } else {
            throw new RuntimeException("InnerHTML value must be string");
        }
        
    }
    
    /**
     * Generates HTML element code using attributes and inner value
     * @return : String
     */
    public function __toString() {
        $str = "<".$this->tag;
        if (sizeof($this->attributes) > 0) {
            foreach($this->attributes as $key => $value) {
                $str .= " ".$key."=\"".$value."\"";
            }
        }
        $str .= ">";
        if ($this->innerHTML != null) {
            $str .= $this->innerHTML;
        }
        $str .= "</".$this->tag.">";
        return $str;
    }
    
    /**
     * Clones targeted HTMLElement object. Can be used like so:
     * $clone = clone $otherObject;
     * 
     * @return : Object clone of targeted HTMLElement object
     */
    public function __clone() {
        $clone = new HTMLElement($this->tag);
        foreach ($this->attributes as $key => $value) {
            $clone->setAttribute($key, $value);
        }
        return $clone;
    }
}
?>