<?php

use AppBundle\Util\DoctrineDataManager;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DataManagerTest extends KernelTestCase {
    
    protected $dataManager;
    protected $firstname;
    protected $lastname;
    protected $email;
    protected $password;
    
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        
        $this->dataManager = new DoctrineDataManager($this->em);
            
        $this->firstname = "Asd";
        $this->lastname = "Asdnen";
        $this->email = "asd.asdnen@metropolia.fi";
        $this->password = "moimoimoimoi";
        
        $this->dataManager->deleteCustomer($this->email);
    }
    
    public function insertTestCustomer() {
        return $this->dataManager->insertCustomer($this->firstname, 
            $this->lastname,
            $this->email,
            $this->password);
    }
    
    public function testEmailInUse() {
        
        // Ensure customer details are in the database
        $this->insertTestCustomer();
        
        $this->assertTrue($this->dataManager->emailInUse($this->email), "Email should have been in use");
    }

    public function testInsertCustomerFirstTime() {
        $response = $this->insertTestCustomer();
        
        $this->assertTrue($response, "Creating a user failed");
    }
    
    public function testInsertCustomerExisting() {
        $this->insertTestCustomer();
        
        $response = $this->insertTestCustomer();
        $this->assertTrue(!$response, "Creating a user failed");
    }
    
    public function testLogIn() {
        $this->insertTestCustomer();
        
        $this->assertTrue($this->dataManager->login($this->email,$this->password), "LogIn failed");
            
    }
    
    public function testDeleteUser(){
        $this->insertTestCustomer();
        
        $response = $this->dataManager->deleteCustomer($this->email);
        $this->assertTrue($response, "Deleting a user failed");
          
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();
        
        if ($this->em != null) {
            $this->em->close();
        }
        $this->em = null; // avoid memory leaks
    }
}