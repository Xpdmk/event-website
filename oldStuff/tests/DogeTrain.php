<?php
phpinfo();
require_once('EventModel.php');
   $databaseManager = new Database();
    $em = $databaseManager->getEntityManager();
    $category = new Event;
    $category->translate('fr')->setName('Chaussures');
    $category->translate('en')->setName('Shoes');
    $em->persist($category);

    // In order to persist new translations, call mergeNewTranslations method, before flush
    $category->mergeNewTranslations();

    echo $category->translate('en')->getName();
    
    ?>