<?php
namespace components\material;

class EventListItem extends HTMLElement {
    
    private $titleElement;
    private $descriptionContainer;
    private $buttonLink;
    
    public function __construct($title, $desciption, $buttonText) {
        parent->tag = "div";
        parent->attributes = ["class" => "demo-card-square mdl-card mdl-shadow--2dp"];
        
        // Create title elements
        $titleElement = new HTMLElement("div", ["class" => "mdl-card__title-text"], $title);
        $this->titleElement = &$titleElement;
        $titleContainer = new HTMLElement("div", ["class" => "mdl-card__title mdl-card--expand"], [$titleElement]);
        
        // Create description container
        $descriptionContainer = new HTMLElement("div", ["class" => "mdl-card__supporting-text"], $description);
        $this->descriptionContainer = &$descriptionContainer;
        
        // Create 
        $buttonLink = new HTMLElement("a", ["class" => "mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"], $this->values["buttonText"]);
        $this->buttonLink = &$buttonLink;
        $buttonContainer = new HTMLElement("div", ["class" => "mdl-card__actions mdl-card--border"], [$buttonLink]);
        parent->innerHTML([$titleContainer, $descriptionContainer, $buttonContainer]);
    }
    
    public function setTitle($title) {
        $this->titleElement->setInnerHTML($title);
    }
    
    public function setDescription($description) {
        $this->descriptionContainer->setInnerHTML($description);
    }
    
    public function setButtonText($buttonText) {
        $this->buttonText->setInnerHTML($buttonText);
    }
}
?>