<?php

interface HTMLFactory_IF {
    public function createMainPage();
    public function createEventPage();
    public function createBrowsePage();
}

class MaterialDesignFactory implements HTMLFactory_IF {

    
    public function createMainPage(){
        /* $json_user = käyttäjän tiedot. Jos ei kirjautunut, $json_user = null*/
        
        $page = new MaterialDesignNavbar($json_user);
        $page .= new MaterialDesignMainPage($json_mainpage);
        
        return $page;
    }
    
    public function createEventPage($json){
        /* $json_user = käyttäjän tiedot. Jos ei kirjautunut, $json_user = null*/
        /* $json_event = eventin tiedot. Jos $json_event = null, näytä virhe sivu*/
        
        $page = new MaterialDesignNavbar($json_user);
        $page .= new MaterialDesignEventPage($json_event);
        
        return $page;
    }
    
    public function createBrowsePage($json) {
        /* $json_user = käyttäjän tiedot. Jos ei kirjautunut, $json_user = null*/
        /* $json_results = hakutulokset. Jos ei tuloksia, näytä virhe*/
        
        $page = new $navbar($json_user);
        $page .= new $browsePage($json_results);
    }
    
    
}

class MaterialDesignNavbar {
    private $profile;
    private $buttons;
    
    public function __construct($json) {
        $this->profile = profile($json);
    }
    
    public function profile($json){
        if($json != null){
            $this->profile = new MaterialDesignProfile();
        } else {
             $this->progile = new MaterialDesignNoProfile();
        }
    }
    
    
    
}


/**
 * Creates list of events.
 */
class MaterialDesignEventList{
    private $listGroup;
    private $listItemsString;
    
    public function __construct($json){
        
        for ($i = 0; $i < sizeof($json); $i++) { /* Creates listitems */
            $event = $json[$i];
            $listItem = new MaterialDesignListItem($event);
            $listItemsString .= $listItem;
        }
        
        /* Inserts listitems in listGroup*/
        $listGroup = new Template("html/listGroup.tpl");
        $listGroup->set("class", "mdl-list");
        $listGroup->set("id", "event-list");  /*En ole varma että tarvitaan*/
        $listGroup->set("value", $listItemsString);
    }
    
    public function __toString(){
        return $this->listgroup;
    }
}

/**
 * Creates Event listitem with material design.
 */
class MaterialDesignEventListItem  {
    private $listItem;
    
    public function __construct($json){
        $imgsrc = "img/".$eventJSON["id"].".png";
        
        $listItem = new Template("html/materialDesignEventListItem.tpl");/*TODO: materialDesignEventListItem.tpl*/
        $listItem->set("title", $eventJSON["title"]);
        $listItem->set("description", $eventJSON["description"]);
        $listItem->set("imageSource", $imgsrc);
        
        $starsString = $this->createStars($eventJSON["reviews"]);
        $listItem->set("stars", $starsString);
    }
    
    public function __toString(){
        return $this->listItem;
    }
    
}

/*
TODO:
finish eventlist
head
eventpage
*/




?>