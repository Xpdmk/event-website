<?php
// TODO: Tee php testit


class Template {
    protected $str;
    protected $values = array();
  
    public function __construct($file, $array = null) {
        if (!file_exists($file)) {
            throw new RuntimeException("File ($file) not found.");
        } else {
            $this->str = file_get_contents($file);
            if ($array != null) {
                if ($this->isAssoc($array, true)) {
                    $this->values = $array;
                }
            }
        }
    }
    
    public function set($key, $value) {
        $this->values[$key] = $value;
    }
    
    public function addPairs($pairs) {
        if ($this->isAssoc($pairs, true)) {
            foreach($pairs as $key => $value) {
                if ($this->values[$key] == null) {
                    $this->values[$key] = $value;
                }
            }
        }
    }
    
    public function setPairs($pairs) {
        if ($this->isAssoc($pairs, true)) {
            $this->values = $pairs;
        }
    }
    
    private function isAssoc(array $arr, $shouldBe = null) {
        if (array() === $arr) return false;
        $isAssoc = array_keys($arr) !== range(0, count($arr) - 1);
        if ($shouldBe != null) {
            if (gettype($shouldBe) == "boolean") {
                if ($isAssoc == $shouldBe) {
                    return true;
                } else {
                    throw new RuntimeException("Given array is not associative.");
                }
            } else {
                throw new RuntimeException("Unexpected value type ".gettype($shouldBe)." given. Expected boolean.");
            }
        } else {
            return $isAssoc;
        }
    }
    
    private function insertValues() {
        $str = $this->str;
        foreach (array_keys($this->values) as $key) {
            $tagToReplace = "[@$key]";
            $str = str_replace($tagToReplace, $this->values[$key], $str);
        }
        return $str;
    }
    
    public function output() { // TODO: Lisää templatesta käyttämättömien [@]-kohtien poisto
        return $this->insertValues();
    }
    
    public function __clone() {
        $clone = new Template($this->file);
        $clone->setPairs($this->values);
        return $clone;
    }
    
    public function __toString() {
        return $this->insertValues();
    }
}
?>