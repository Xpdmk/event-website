<?php

/**
 * Tool function for loading file dependences for files. File used by a file are configured in dependence.json file
 */
function loadDependences($file) {
    $file = basename($file);
    $pathToDependenceFile = __DIR__."/dependences.json";
    
    // Load dependences.json
    $str = file_get_contents($pathToDependenceFile);
    $json = json_decode($str, true);
    
    // Require files
    $fileDependences = $json[$file];
    if ($fileDependences != null) {
        foreach ($fileDependences as $dependence) {
            $path = __DIR__."/../".$dependence["path"];
            // echo "Loaded: ".$path."<br>";
            require_once($path);
        }
    }
}
?>