<?php

interface HTMLBuilder_IF {
    public function createMainPage($json);
    public function createEventPage($json);
    public function createSearchPage($json);
    public function getElements($json);
}

?>