var searchHandler = {
    setResults: function(sr) {
        if(this.searchResults == null){
        this.searchResults = sr;}
        else{
            return sr;
        }
    },
    getResults: function() {
        return this.searchResults;
    }
};



var contentUpdater = {

    clearContainer: function() {
        var container = this.getContainer();
        while (container.firstChild) {
            container.removeChild(container.firstChild);
        }

    },
    insertCode: function(html) {
        var container = this.getContainer();
        this.clearContainer();
        container.innerHTML = html;

    },

    getContainer: function() {
        return document.getElementById("container");
    },
    setContainer: function() {

    }

};



/**
 * Hides the navbar when scrolling down and shows it when scrolling up.
 * 
 * @return {Void}
 **/
function hideOnScroll() {
    window.addEventListener('scroll', function(e) {
        var distanceY = window.pageYOffset || document.documentElement.scrollTop;
        var hideOn = 300;
        if (distanceY > hideOn) {

            document.getElementById("navbar").style.visibility = "hidden";
        }
        else {
            if (document.getElementById("navbar").style.visibility == "hidden") {
                document.getElementById("navbar").style.visibility = "visible";
            }
        }
    });
}
