var requester = {
    /**
     *  Handles all the actions that require operating the database in any way.
     *  @param {String} action An action we want to run.
     **/
    useDB: function(action) {
        switch (action) {
            case 'get_10Newest':
                this.makeRequest("10Events", "Starter.php"); //Starter.php
                break;
            case 'get_allEvents':
                this.makeRequest("get_allEvents");
                break;
            case 'get_eventNames':
                this.makeRequest("get_eventNames", "Starter.php");
                break;
            case 'get_event':
                this.makeRequest("event", "EventController.php");
                break;
            case 'get_searchResources':
                return this.makeRequest("searchResources", "EventController.php");

            case 'get_carouselImages':
                this.makeRequest("carouselImages", "EventController.php");
                break;
            case 'get_searchResults':
                this.makeRequest("searchEvents", "EventController.php"); //Starter.php
                break;
            default:
                // code
        }
    },
    /**
     *  Sends and handles all AJAX requests.
     *  @param {String} action An action we want to commit.
     *  @param {Function} callback A callback function we want to start after the AJAX gets its response.
     *  @param {String} file The file we want to use in the URL.
     **/
    makeRequest: function(action, file) {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            console.log(this.readyState + " " + this.status);
            if (this.readyState == 4 && this.status == 200) {
                var response = this.responseText;
                console.log(action + "'s result is " + typeof(response) + ":\n" + response);
                if (typeof(response) == "string") {
                    response = JSON.parse(response);
                    console.log(action + " JSON:\n");
                    console.log(response);
                }
                return response;
            }
        };
        var url;
        if (action == "carouselImages") {
            url = file + "/" + action + "/" + this.json.id;
        }
        else if (action == "searchEvents") {
            var ids = "";
            if (searchResults != null) {
                for (var i = 0, len = searchResults.length; i < len; i++) {
                    ids += searchResults[i].id + ",";
                }
                ids = ids.slice(0, -1);
            }
            console.log(file + "/" + action + "?q=" + ids);
            url = file + "/" + action + "?q=" + ids; //type a head ehdotusten idt;
        }
        else {
            url = file + "/" + action;
        }
        xmlhttp.open("GET", url, true);
        xmlhttp.send();

    }
};



function setSearchResources(json) {
    setupBloodhound(json);
}
