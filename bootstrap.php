<?php
// bootstrap.php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
require_once "vendor/autoload.php";

// Create a simple "default" Doctrine ORM configuration for Annotations
$isDevMode = true;
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/src/AppBundle/Entity"), $isDevMode);
// database configuration parameters
$dbParams = array(
    'driver'   => 'pdo_mysql',
    'user'     => 'admin',
    'password' => 'ryhma10',
    'dbname'   => 'event_db',
);

// obtaining the entity manager
$entityManager = EntityManager::create($dbParams, $config);
?>