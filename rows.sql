USE event_db;
INSERT INTO Event (name, description, linkToTrailer) VALUES ("Arrival",
    "Arrival on ohjaaja Denis Villeneuven (Sicario, Vangitut) ajatuksia herättävä science fiction -trilleri. Kun salaperäisiä avaruusaluksia laskeutuu ympäri maapalloa kielitieteilijä Louise Banksin (Amy Adams) johtama eliittitiimi kutsutaan tutkimaan. ",
    "https://youtu.be/3GSqOUTpfeI"
);
INSERT INTO Event (name, description, linkToTrailer) 
    VALUES ("La La Land", "Here is for the fools who dream.\nLa La Land -elokuva kertoo aloittelevan näyttelijän Mian (Emma Stone) ja omistautuneen jazz-muusikon Sebastianin (Ryan Gosling) tarinan. He ponnistelevat unelmiensa eteen kaupungissa, joka tunnetusti tuhoaa toiveet ja särkee sydämet. Nykyhetken Los Angelesin päivittäisestä elämästä kertova alkuperäismusikaali tutustuttaa unelmien tavoittelun iloihin ja suruihin.\nOscar-®ehdokas Damien Chazellen (Whiplash) käsikirjoittama ja ohjaama La La Land on rakkaudenosoitus enkelten kaupungille ja Hollywood-musikaaleille.", "https://www.youtube.com/watch?v=0pdqf4P9MB8");
INSERT INTO Event (name, description, linkToTrailer) VALUES("Assassin's Creed",
    "Assassin’s Creed perustuu suosittuun toimintapeliin, joka on saanut vahvasti vaikutteita oikeista historian tapahtumista, henkilöistä ja paikoista.\nMullistavan teknologian avulla Callum Lynch (Michael Fassbender) käy läpi esi-isänsä, Aguilarin (Michael Fassbander) kokemuksia inkvisition ajan Espanjassa 500 vuotta sitten. Callum saa kuulla kuuluvansa mystiseen salaseuraan, assassiineihin eli salamurhaajiin – ja hänen toisessa aikakaudessa keräämiään tietoja ja taitoja tarvitaan taistelussa mahtavaa ja tyrannimaista Templar-organisaatiota vastaan.",
    "https://www.youtube.com/watch?v=0fITmuqh4g8");
INSERT INTO Event (name, description, linkToTrailer) VALUES("Life, Animated",
    "Life, Animated kertoo autistisesta Owen Suskindista, joka oppi puhumaan Disney-animaatioiden kautta. Owenin vanhemmat kertovat oman puolensa pojan lapsuusajasta, ja hänen omat kokemuksensa tuolta ajalta on osin animoitu. Owen kertoo tietenkin myös itse elämästään ja kasvustaan autistisesta lapsesta itsenäiseksi mieheksi, joka uskoo tulevaisuuteen ja haluaa vain elää hyvä elämän, kuten me kaikki muutkin.",
    "https://www.youtube.com/watch?v=4n7fosK9UyY");
    
INSERT INTO Customer (firstName, lastName, email) VALUES ("Pate", "Esimerkki", "pate.esimerkki@gmail.com");
INSERT INTO Customer (FirstName, lastName, email) VALUES ("Erkki", "Esimerkki", "erkki.esimerkki@gmail.com");
INSERT INTO Customer (FirstName, lastName, email) VALUES ("Tuukka", "Esimerkki", "tuukka.esimerkki@gmail.com");

INSERT INTO Review (eventId, customerId, stars) VALUES (1, 1, 2);
INSERT INTO Review (eventId, customerId, stars) VALUES (2, 1, 5);
INSERT INTO Review (eventId, customerId, stars) VALUES (3, 1, 3);
INSERT INTO Review (eventId, customerId, stars) VALUES (4, 1, 4);
INSERT INTO Review (eventId, customerId, stars) VALUES (1, 2, 5);

insert into Presentation(startDate, endDate, startTime, endTime, eventId) values ("2017-02-27", "2017-02-27", "18:00:00", "19:00:00", 1);
insert into Presentation(startDate, endDate, startTime, endTime, eventId) values ("2017-02-27", "2017-02-27", "18:00:00", "19:50:00", 1);
insert into Presentation(startDate, endDate, startTime, endTime, eventId) values ("2017-02-27", "2017-02-27", "18:00:00", "19:00:00", 1);
insert into Presentation(startDate, endDate, startTime, endTime, eventId) values ("2017-02-27", "2017-02-27", "18:00:00", "19:00:00", 2);
insert into Presentation(startDate, endDate, startTime, endTime, eventId) values ("2017-02-27", "2017-02-27", "18:00:00", "18:00:00", 1);

INSERT INTO DiscountGroup(name) VALUES ("Aikuinen");
INSERT INTO DiscountGroup(name) VALUES ("Lapsi");
INSERT INTO DiscountGroup(name) VALUES ("Opiskelija");
INSERT INTO DiscountGroup(name) VALUES ("Huoltaja");
INSERT INTO DiscountGroup(name) VALUES ("Yleinen");
INSERT INTO DiscountGroup(name) VALUES ("Rajoittunut");

INSERT INTO Location(streetAddress,postalCode) VALUES ("Mannerheimintie 50",00440);
INSERT INTO Location(streetAddress,postalCode) VALUES ("Kaanintie 4",00540);
INSERT INTO Location(streetAddress,postalCode) VALUES ("Jasperkuja 5",00340);

INSERT INTO PostalCode(postalCode,city) VALUES ("00440","Helsinki");
INSERT INTO PostalCode(postalCode,city) VALUES ("00540","Vihti");
INSERT INTO PostalCode(postalCode,city) VALUES ("00340","Espoo");

INSERT INTO Space(spaceName,locationId) VALUES ("Sali 1",1);
INSERT INTO Space(spaceName,locationId) VALUES ("Sali 2",1);
INSERT INTO Space(spaceName,locationId) VALUES ("Sali 3",1);
INSERT INTO Space(spaceName,locationId) VALUES ("Sali 4",1);
INSERT INTO Space(spaceName,locationId) VALUES ("Näytöshalli",2);
INSERT INTO Space(spaceName,locationId) VALUES ("Frederikin tori",2);
INSERT INTO Space(spaceName,locationId) VALUES ("Kerros 3",3);
INSERT INTO Space(spaceName,locationId) VALUES ("Kerros 7",3);
INSERT INTO Space(spaceName,locationId) VALUES ("Kerros 2",3);

INSERT INTO Seat(seatRow,seatNumber,seatType,spaceId) VALUES (1,1,1,1);
INSERT INTO Seat(seatRow,seatNumber,seatType,spaceId) VALUES (1,2,4,1);
INSERT INTO Seat(seatRow,seatNumber,seatType,spaceId) VALUES (1,2,5,1);

INSERT INTO SeatType(seatTypeName) VALUES ("Istumapaikka");
INSERT INTO SeatType(seatTypeName) VALUES ("Seisomapaikka");
INSERT INTO SeatType(seatTypeName) VALUES ("Pyörätuolipaikka");
INSERT INTO SeatType(seatTypeName) VALUES ("Pöytäpaikka");
INSERT INTO SeatType(seatTypeName) VALUES ("Keinutuolipaikka");

INSERT INTO SeatPricing(price,max,discountGroupId,presentationId,seatTypeId) VALUES (100,30,5,1,2);
INSERT INTO SeatPricing(price,max,discountGroupId,presentationId,seatTypeId) VALUES (30,100,5,3,1);
INSERT INTO SeatPricing(price,max,discountGroupId,presentationId,seatTypeId) VALUES (100,30,3,2,1);
INSERT INTO SeatPricing(price,max,discountGroupId,presentationId,seatTypeId) VALUES (100,30,5,3,2);
INSERT INTO SeatPricing(price,max,discountGroupId,presentationId,seatTypeId) VALUES (100,30,2,4,2);

INSERT INTO Ticket(seat,customerId,seatPricing) VALUES (1,1,1);
INSERT INTO Ticket(seat,customerId,seatPricing) VALUES (1,1,3);
INSERT INTO Ticket(seat,customerId,seatPricing) VALUES (1,1,4);
INSERT INTO Ticket(seat,customerId,seatPricing) VALUES (1,2,4);









