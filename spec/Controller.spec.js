describe('Controller tests', function() {

  beforeEach(function() {
    var container = document.getElementById("container");
    var header = document.createElement("h1");
    container.appendChild(header);
    console.log("moi");
  });
  describe('Builder module', function() {


    it('should clear container', function() {
      contentUpdater.clearContainer();
      expect(container.firstChild).toBe(null);
    });

    it('should add html to website', function() {

      contentUpdater.insertCode('<h1></h1>');
      expect(container.firstChild.tagName).toBe('H1');
    });

    it('should return the container', function() {
      expect(contentUpdater.getContainer()).toBe(container);

    });

  });
  describe('AJAX module', function() {


    it('should return ten newest events', function() {
       expect(container.firstChild).not.toEqual(null);
       pending();
   
    });

    it('should return all events ', function() {
        expect(container.firstChild).not.toEqual(null);
        pending();
    });

    it('should return a single event by its id', function() {
        expect(container.firstChild).not.toEqual(null);
        pending();
    });
     it('should return events as search resources', function() {
      pending();
    });
     it('should return carousel images', function() {
      pending();
    });  
    it('should return searched events', function() {
      pending();
    }); 

  });
});
          